package Services;

import DAO.ModelDAO;
import Entity.Model;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by minhthuc on 3/23/2017.
 */
@Path("/userServices")
public class ModelServices {
    private ModelDAO modelDAO = new ModelDAO();
    @GET
    @Path("/model")
    @Produces(MediaType.APPLICATION_XML)
    public ArrayList<Model> getModel() throws SQLException {
        return modelDAO.getAllModel();
    }
}

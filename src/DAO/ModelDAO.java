package DAO;

import Entity.Model;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthuc on 3/23/2017.
 */
public class ModelDAO {

    private Model m;
    public ArrayList<Model> getAllModel() throws SQLException {
        ArrayList<Model> lim = new ArrayList<Model>();
        Statement stml = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Db connected");
        Connection connection = null;
        try{
            connection = DriverManager.getConnection("jdbd:mysql://localhost:3306/model","root","");
        }catch (SQLException e){
            System.out.println("connect fail");
            e.printStackTrace();
        }
        if(connection!=null){
            String query = "Select * from Model";
            try{
                stml = connection.createStatement();
                ResultSet rs = stml.executeQuery(query);
                while (rs.next()){
                    int id = Integer.parseInt(rs.getString("user_id"));
                    String username = rs.getString("user_name");
                    int inta = Integer.parseInt(rs.getString("instagram_id"));
                    String password = rs.getString("password");
                    int follow = Integer.parseInt(rs.getString("follows"));
                    String followed_by = rs.getString("followed_by");
                    String gender = rs.getString("gender");
                    int fee_tw = Integer.parseInt(rs.getString("fee_tw"));
                    int fee_salon = Integer.parseInt(rs.getString("fee_salon"));
                    int fee_web = Integer.parseInt(rs.getString("fee_web"));
                    int id_int = Integer.parseInt(rs.getString("id_init"));
                    int id_album = Integer.parseInt(rs.getString("id_album"));
                    m = new Model(id,inta,username,password,follow,followed_by,gender,fee_tw,fee_web,fee_salon,id_int,id_album);
                    lim.add(m);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if(stml!=null){
                    stml.close();
                }
            }
        }
        return lim;
    }
}

package Entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Created by minhthuc on 3/23/2017.
 */
@XmlRootElement(name = "model")
public class Model implements Serializable {
    private static final long serialVersionUID=1;
    private int id;
    private int intagram_id;
    private String username;
    private String password;
    private int folows;
    private String followedBy;
    private String gender;
    private int fee_tv;
    private int fee_web;
    private int fee_salon;
    private int id_init;
    private int id_album;

    public Model() {
    }

    public Model(int id, int intagram_id, String username, String password, int folows, String followedBy, String gender, int fee_tv, int fee_web, int fee_salon, int id_init, int id_album) {
        this.id = id;
        this.intagram_id = intagram_id;
        this.username = username;
        this.password = password;
        this.folows = folows;
        this.followedBy = followedBy;
        this.gender = gender;
        this.fee_tv = fee_tv;
        this.fee_web = fee_web;
        this.fee_salon = fee_salon;
        this.id_init = id_init;
        this.id_album = id_album;
    }

    public int getIntagram_id() {
        return intagram_id;
    }
    @XmlElement
    public void setIntagram_id(int intagram_id) {
        this.intagram_id = intagram_id;
    }

    public String getPassword() {
        return password;
    }
    @XmlElement
    public void setPassword(String password) {
        this.password = password;
    }

    public int getFolows() {
        return folows;
    }
    @XmlElement
    public void setFolows(int folows) {
        this.folows = folows;
    }

    public String getFollowedBy() {
        return followedBy;
    }
    @XmlElement
    public void setFollowedBy(String followedBy) {
        this.followedBy = followedBy;
    }

    public String getGender() {
        return gender;
    }
    @XmlElement
    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getFee_tv() {
        return fee_tv;
    }
    @XmlElement
    public void setFee_tv(int fee_tv) {
        this.fee_tv = fee_tv;
    }

    public int getFee_web() {
        return fee_web;
    }
    @XmlElement
    public void setFee_web(int fee_web) {
        this.fee_web = fee_web;
    }

    public int getFee_salon() {
        return fee_salon;
    }
    @XmlElement
    public void setFee_salon(int fee_salon) {
        this.fee_salon = fee_salon;
    }

    public int getId_init() {
        return id_init;
    }
    @XmlElement
    public void setId_init(int id_init) {
        this.id_init = id_init;
    }

    public int getId_album() {
        return id_album;
    }
    @XmlElement
    public void setId_album(int id_album) {
        this.id_album = id_album;
    }

    public int getId() {
        return id;
    }
    @XmlElement
    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    @XmlElement
    public void setUsername(String username) {
        this.username = username;
    }
}
